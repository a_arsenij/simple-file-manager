module.exports = {
  "extends": ["../.eslintrc.cjs"],
  "plugins": [
    "react",
    "react-hooks"
  ],
  "rules": {
    "react/jsx-equals-spacing": ["error", "never"],
    "react-hooks/rules-of-hooks": "error",
    "react-hooks/exhaustive-deps": "error"
  }
};
