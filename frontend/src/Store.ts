import { configureStore } from "@reduxjs/toolkit";

import { filesSlice } from "./FilesSlice";
export const store = configureStore({
    reducer: {
        files: filesSlice.reducer,
    },
});

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
