export type FilePreviewData = {
    filename: string,
    isFolder: boolean,
    size: number,
}

export type FilePreview = FilePreviewData & {
    selected: boolean,
}
