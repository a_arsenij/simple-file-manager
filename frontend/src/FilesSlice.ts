import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import { AppDispatch, RootState } from "./Store";
import { FilePreview, FilePreviewData } from "./types/FilePreview";

type FilesState = {
    path: string,
    files: FilePreview[],
    status: "loading" | "completed" | "failed",
    sortCriteria: "name" | "size",
    sortOrder: "asc" | "desc",
}

const initialState: FilesState = {
    path: "",
    files: [],
    status: "loading",
    sortCriteria: "name",
    sortOrder: "asc",
};

export const fetchFiles = (path: string) => (dispatch: AppDispatch, getState: () => RootState) => {
    dispatch(filesSlice.actions.startLoading(path));
    fetch(`/api/getFilesList?path=${encodeURIComponent(path)}`)
        .then((res) => res.json())
        .then((res) => res as FilePreviewData[])
        .then((res) => {
            const state = getState();
            if (state.files.path === path) {
                dispatch(filesSlice.actions.completeLoading(res.map((item) => ({
                    ...item,
                    selected: false,
                }))));
            }
        })
        .catch(() => {
            dispatch(filesSlice.actions.failLoading());
        });
};

export const navigateToSubfolder = (folderName: string) => (dispatch: AppDispatch, getState: () => RootState) => {
    const state = getState();
    const newPath = `${state.files.path}/${folderName}`;
    dispatch(fetchFiles(newPath));
};

function downloadFileByFullPath(path: string) {
    const aElement = document.createElement("a");
    aElement.setAttribute("download", path.split("/").at(-1)!);
    aElement.href = `/api/getFile?path=${encodeURIComponent(path)}`;
    aElement.setAttribute("target", "_blank");
    aElement.click();
}

export const filesSlice = createSlice({
    name: "files",
    initialState,
    reducers: {
        startLoading: (state, path: PayloadAction<string>) => {
            state.path = path.payload;
            state.files = [];
            state.status = "loading";
        },
        completeLoading: (state, action: PayloadAction<FilePreview[]>) => {
            state.files = action.payload;
            state.status = "completed";
        },
        failLoading: (state) => {
            state.files = [];
            state.status = "failed";
        },
        selectFile: (state, fileName: PayloadAction<string>) => {
            const file = state.files.find((f) => f.filename === fileName.payload);
            if (file) {
                file.selected = !file.selected;
            }
        },
        selectAllFiles: (state) => {
            const mostAreSelected = state.files.length === 0
                ? false
                : state.files.filter((f) => f.selected).length / state.files.length > 0.5
            ;
            state.files.forEach((f) => {
                f.selected = !mostAreSelected;
            });
        },
        setSortingOrder: (state, order: PayloadAction<FilesState["sortOrder"]>) => {
            state.sortOrder = order.payload;
        },
        setSortingCriteria: (state, criteria: PayloadAction<FilesState["sortCriteria"]>) => {
            state.sortCriteria = criteria.payload;
        },
        downloadFile: (state, fileName: PayloadAction<string>) => {
            downloadFileByFullPath(`${state.path}/${fileName.payload}`);
        },
        downloadSelectedFiles: (state) => {
            state.files
                .filter((f) => !f.isFolder && f.selected)
                .forEach((f) => downloadFileByFullPath(`${state.path}/${f.filename}`));
        },
    },
});

export const {
    startLoading,
    completeLoading,
    failLoading,
    selectFile,
    selectAllFiles,
    setSortingOrder,
    setSortingCriteria,
    downloadFile,
    downloadSelectedFiles,
} = filesSlice.actions;

export function selectSorted(state: RootState): FilePreview[] {
    const res = [...state.files.files].sort((a, b) => {
        if (a.isFolder && !b.isFolder) {
            return -1;
        }
        if (b.isFolder && !a.isFolder) {
            return 1;
        }
        switch (state.files.sortCriteria) {
            case "name":
                return a.filename.localeCompare(b.filename);
            case "size":
                return a.size - b.size;
        }
    });
    if (state.files.sortOrder === "desc") {
        res.reverse();
    }
    return res;
}
