import { IconProp } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";

import styles from "./Button.module.css";

type Props = {
    title: string,
    icon?: IconProp,
    onClick: () => void,
    disable?: boolean,
}

export function Button({
    title,
    icon,
    onClick,
    disable = false,
}: Props) {
    return <button
        className={styles.button}
        onClick={onClick}
        disabled={disable}
    >
        {icon && <FontAwesomeIcon icon={icon}/> }
        {title}
    </button>;
}
