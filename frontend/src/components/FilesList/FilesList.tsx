import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";

import { downloadFile, navigateToSubfolder, selectFile, selectSorted } from "../../FilesSlice";
import { useAppDispatch, useAppSelector } from "../../hooks";
import { clsx } from "../../utils/Clsx";
import { bytesToPrefixes } from "../../utils/Size";
import styles from "./FilesList.module.css";

export function FilesList() {
    const files = useAppSelector(selectSorted);
    const dispatch = useAppDispatch();

    return <div className={styles.filesList}>
        {files.map((f) =>
            <div className={clsx(
                styles.item,
                !f.isFolder && styles.file,
                f.isFolder && styles.folder,
            )} key={f.filename}>
                <div className={styles.icon}>
                    {!f.isFolder && <button
                        className={clsx(
                            styles.select,
                            f.selected && styles.selected,
                            !f.selected && styles.unselected,
                        )}
                        onClick={() => dispatch(selectFile(f.filename))}
                    >
                        {f.selected && <FontAwesomeIcon icon={["fas", "square-check"]}/>}
                        {!f.selected && <FontAwesomeIcon icon={["far", "square"]}/>}
                    </button> }
                    {f.isFolder && <FontAwesomeIcon icon={["fas", "folder"]}/>}
                </div>
                <button
                    className={styles.name}
                    onClick={ () => {
                        if (f.isFolder) {
                            dispatch(navigateToSubfolder(f.filename));
                        } else {
                            dispatch(downloadFile(f.filename));
                        }
                    }}
                >
                    {f.filename}
                </button>
                {!f.isFolder && <div className={styles.size}>
                    {bytesToPrefixes(f.size)}
                </div>}
            </div>,
        )}
    </div>;
}
