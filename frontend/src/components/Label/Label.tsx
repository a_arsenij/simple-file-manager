import { IconProp } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";

import styles from "./Label.module.css";

type Props = {
    title: string,
    icon: IconProp,
}

export function Label({
    title,
    icon,
}: Props) {
    return <div
        className={styles.label}
    >
        {icon && <FontAwesomeIcon icon={icon}/> }
        {title}
    </div>;
}
