import React, { useEffect } from "react";

import { fetchFiles } from "../../FilesSlice";
import { useAppDispatch, useAppSelector } from "../../hooks";
import { Breadcrumbs } from "../Breadcrumbs/Breadcrumbs";
import { FilesList } from "../FilesList/FilesList";
import { Label } from "../Label/Label";
import { Toolbar } from "../Toolbar/Toolbar";
import styles from "./App.module.css";

function App() {
    const dispatch = useAppDispatch();

    useEffect(() => {
        dispatch(fetchFiles(""));
    }, [dispatch]);

    const status = useAppSelector((state) => state.files.status);

    return <div className={styles.app}>
        <Breadcrumbs/>
        {status === "loading" && <Label title={"Loading"} icon={["fas", "hourglass"]}/>}
        {status === "failed" && <Label title={"Failed"} icon={["fas", "triangle-exclamation"]}/>}
        {status === "completed" && <>
            <Toolbar/>
            <FilesList/>
        </>}
    </div>;
}

export default App;
