import React from "react";

import { fetchFiles } from "../../FilesSlice";
import { useAppDispatch, useAppSelector } from "../../hooks";
import { Button } from "../Button/Button";
import styles from "./Breadcrumbs.module.css";

export function Breadcrumbs() {
    const dispatch = useAppDispatch();
    const path = useAppSelector((state) => state.files.path);
    const parts = path.split("/");

    const pickSubpath = (idx: number) => {
        const subpath = parts
            .filter((_, i) => i <= idx)
            .join("/");
        dispatch(fetchFiles(subpath));
    };

    return <div className={styles.breadcrumbs}>
        {parts.map((p, idx) =>
            <React.Fragment key={`${p}:${idx}`}>
                <Button
                    title={idx === 0 ? "." : p}
                    onClick={() => pickSubpath(idx)}
                />
                <div className={styles.separator}>/</div>
            </React.Fragment>,
        )}
    </div>;
}
