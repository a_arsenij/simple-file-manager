import React from "react";

import { downloadSelectedFiles, selectAllFiles, setSortingCriteria, setSortingOrder } from "../../FilesSlice";
import { useAppDispatch, useAppSelector } from "../../hooks";
import { Button } from "../Button/Button";
import styles from "./Toolbar.module.css";

export function Toolbar() {
    const dispatch = useAppDispatch();

    const isSomeFileSelected = useAppSelector((state) => state.files.files.some((f) => f.selected));
    const criteria = useAppSelector((state) => state.files.sortCriteria);
    const order = useAppSelector((state) => state.files.sortOrder);

    return <div className={styles.toolbar}>

        <Button
            title={order === "asc" ? "Ascending" : "Descending"}
            icon={order === "asc" ? ["fas", "arrow-down-short-wide"] : ["fas", "arrow-down-wide-short"]}
            onClick={() => {
                dispatch(setSortingOrder(order === "asc" ? "desc" : "asc"));
            }}
        />

        <Button
            title={criteria === "name" ? "By name" : "By size"}
            icon={criteria === "name" ? ["fas", "arrow-down-a-z"] : ["fas", "arrow-down-1-9"]}
            onClick={() => {
                dispatch(setSortingCriteria(criteria === "name" ? "size" : "name"));
            }}
        />

        <Button
            title={"Select all"}
            icon={["fas", "check"]}
            onClick={() => {
                dispatch(selectAllFiles());
            }}
        />

        <Button
            title={"Download selected"}
            icon={["fas", "download"]}
            onClick={() => dispatch(downloadSelectedFiles())}
            disable={!isSomeFileSelected}
        />

    </div>;
}
