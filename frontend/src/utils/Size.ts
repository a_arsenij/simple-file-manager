const SUFFIXES = ["B", "KiB", "MiB", "GiB", "TiB", "PiB"];
const UNIT_BASE = 1024;

export function bytesToPrefixes(
    size: number,
): string {
    let suffix = 0;
    let remaining = size;
    while (remaining > UNIT_BASE) {
        suffix++;
        remaining /= UNIT_BASE;
    }
    return `${Math.trunc(remaining)} ${SUFFIXES[suffix]}`;
}
