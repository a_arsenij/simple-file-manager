type ClsxArg =
    | string
    | undefined
    | null
    | false
    | ClsxArg[];

export const clsx: (...arg: ClsxArg[]) => string = (
    ...arg: ClsxArg[]
) => {
    return clsxOrigin(arg);
};

export const clsxOrigin: (arg: ClsxArg) => string = (
    arg: ClsxArg,
) => {
    if (typeof arg === "string") {
        return arg;
    }
    if (arg === undefined) {
        return "";
    }
    if (arg === null) {
        return "";
    }
    if (arg === false) {
        return "";
    }
    if (Array.isArray(arg)) {
        return arg
            .map((a) => clsxOrigin(a))
            .filter((s) => s)
            .join(" ");
    }

    throw new Error(`Typescript is not smart enough to infer that ifs set is exhaustive:\n${JSON.stringify(arg)}`);
};
