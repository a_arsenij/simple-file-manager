import express, { Request, Response } from "express";
import fs from "fs";

process.on("uncaughtException", (error) => {
    console.error(error);
});

const app = express();
const port = 3000;
const root = process.argv[2] || process.cwd();

console.log(`Serving content from ${root}`);

function isPathOk(
    req: Request,
    res: Response,
) {
    const path = req.query.path;
    if (typeof path !== "string") {
        res
            .status(400)
            .end("Path is not specified");
        return undefined;
    }
    if (path.includes("..")) {
        res
            .status(400)
            .end("Path contains prohibited sequences");
        return undefined;
    }
    return path;
}

app.get("/api/getFilesList", async(req, res) => {
    const path = isPathOk(req, res);
    if (path === undefined) {
        return;
    }
    const files = await fs.promises.readdir(root + path);
    const details = await Promise.all(files.map((f) =>
        fs.promises.stat(`${root + path}/${f}`),
    ));
    return res
        .status(200)
        .end(JSON.stringify(
            files.map((f, idx) => {
                const detail = details[idx]!;
                return {
                    filename: f,
                    isFolder: detail.isDirectory(),
                    size: detail.size,
                };
            }),
        ));
});

app.get("/api/getFile", async(req, res) => {
    const path = isPathOk(req, res);
    if (path === undefined) {
        return;
    }
    return res.download(root + path);
});

app.use(express.static("../frontend/dist/"));

app.listen(port, () => {
    console.log(`App is running at http://0.0.0.0:${port}`);
});
